package it.KitchenInLove.ProjectWork.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "StatoOrdini")
public class StatoOrdine {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private int idStatoOrdine;
	
	private String descrizione;

	public StatoOrdine() {
	}

	public StatoOrdine(int idStatoOrdine, String descrizione) {
		super();
		this.idStatoOrdine = idStatoOrdine;
		this.descrizione = descrizione;
	}

	public int getIdStatoOrdine() {
		return idStatoOrdine;
	}

	public void setIdStatoOrdine(int idStatoOrdine) {
		this.idStatoOrdine = idStatoOrdine;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	@Override
	public String toString() {
		return "StatoOrdine [idStatoOrdine=" + idStatoOrdine + ", descrizione=" + descrizione + "]";
	}
	
	
}
